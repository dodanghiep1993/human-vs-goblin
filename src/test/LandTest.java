import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LandTest {

    Land land;

    @BeforeEach
    void setUp() {
        land = new Land();
    }

    @Test
    void printGame() {
        String[][] expected = {{"\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32"},
                {"\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33"},
                {"\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32"},
                {"\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33"},
                {"\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32"},
                {"\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33"},
                {"\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32"},
                {"\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33"},
                {"\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32"}};
        expected[1][1] = "\uD83E\uDD34";
        String[][] actual = {{"\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32"},
                {"\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33"},
                {"\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32"},
                {"\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33"},
                {"\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32"},
                {"\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33"},
                {"\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32"},
                {"\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33"},
                {"\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32"}};
        assertArrayEquals(expected, land.PrintGame(actual, "n", 1, 1));
    }

    @Test
    void printgrid() {
        String[][] expected = {{"\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32"},
                {"\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33"},
                {"\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32"},
                {"\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33"},
                {"\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32"},
                {"\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33"},
                {"\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32"},
                {"\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33"},
                {"\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32"}};
        String[][] actual = {{"\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32"},
                {"\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33"},
                {"\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32"},
                {"\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33"},
                {"\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32"},
                {"\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33"},
                {"\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32"},
                {"\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33"},
                {"\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32"}};
        for (String[] strings: expected) {
            for (String string: strings) {
                System.out.println(string + "\t");
                assertEquals(string + "\t", land);
            }
        }
    }

    @Test
    void getLand() {
        String[][] expected = {{"\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32"},
                {"\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33"},
                {"\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32"},
                {"\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33"},
                {"\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32"},
                {"\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33"},
                {"\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32"},
                {"\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33"},
                {"\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32"}};
        assertArrayEquals(expected, land.getLand());
    }

    @Test
    void testToString() {
        assertEquals("Land has " + "\uD83C\uDF32" + "and" + "\uD83C\uDF33", land.toString());
    }
}