import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HumanTest {

    Human human;
    @BeforeEach
    void setUp() {
        human = new Human(22, 75);
    }

    @Test
    void getHealth() {
        assertEquals(22, human.getHealth());
    }

    @Test
    void getAttackdamge() {
        assertEquals(75,human.getAttackdamge());
    }

    @Test
    void testToString() {
        assertEquals("I am Human: " + "\uD83E\uDD34", human.toString());
    }

    @Test
    void setAttackdamge() {
        human.setAttackdamge(22);
        assertEquals(22, human.getAttackdamge());
    }

    @Test
    void setHealth() {
        human.setHealth(44);
        assertEquals(44, human.getHealth());
    }
}