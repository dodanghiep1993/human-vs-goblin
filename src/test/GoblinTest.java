import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.*;

class GoblinTest {

    Goblin goblin;
    @BeforeEach
    void setUp() {
        goblin = new Goblin(12, 65);
    }

    @Test
    void getHealth() {
        assertEquals(12, goblin.getHealth());
    }

    @Test
    void getAttackdamge() {
        assertEquals(65, goblin.getAttackdamge());
    }

    @Test
    void testToString() {
        assertEquals("I am Goblin: " + "\uD83D\uDC7A", goblin.toString());
    }
}