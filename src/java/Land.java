

public class Land {
    private String[][] land = {{"\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32"},
        {"\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33"},
        {"\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32"},
        {"\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33"},
        {"\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32"},
        {"\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33"},
        {"\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32"},
        {"\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33", "⬜", "\uD83C\uDF33"},
        {"\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32", "\uD83C\uDF32"}};
    public Land () {
    }

    //This method will place human symbol on the grid according to the user input.
    public static String[][] PrintGame(String[][] grid, String move, int x, int y) { ;
        switch (move) {
            case "n":
                grid[x][y] = "\uD83E\uDD34";
                grid[x + 2][y] = "⬜";
                break;
            case "s":
                grid[x ][y] = "\uD83E\uDD34";
                grid[x - 2][y] = "⬜";
                break;
            case "e":
                grid[x][y] = "\uD83E\uDD34";
                grid[x][y - 2] = "⬜";
                break;
            case "w":
                grid[x][y] = "\uD83E\uDD34";
                grid[x][y + 2] = "⬜";
                break;
        }
        return grid;
    }

    //This method will print out the grid in the game world.
    public static void Printgrid(String[][] grid) {
        for (String[] array: grid) {
            for(String str: array) {
                System.out.print(str + "\t");
            }
            System.out.println();
        }
    }

    public String[][] getLand() {
        return land;
    }

    @Override
    public String toString() {
        return "Land has " + "\uD83C\uDF32" + "and" + "\uD83C\uDF33";
    }
}


