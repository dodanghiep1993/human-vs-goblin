

import java.util.Random;
import java.util.Scanner;

public class GameWorld {
    public static void main(String[] args) {
        Scanner getInput = new Scanner(System.in);
        Scanner attack = new Scanner(System.in);
        Random rand = new Random();

        //Creating goblin, human and land objects
        Goblin g = new Goblin(75, 25);
        Human h = new Human(100, 50);
        Land l = new Land();

        //Randomly generate goblin's position when the game start
        int[] xarray = new int[]{1,3,5,7};
        int[] yarray = new int[]{1,3,5,7,9};
        int xg = xarray[rand.nextInt(xarray.length)];
        int yg = yarray[rand.nextInt(yarray.length)];

        if (xg == 1 && yg == 1) {
            yg = 3;
        }

        //initialize human's position at grid[1][1]
        int x = 1;
        int y = 1;

        System.out.println("\n" + "############################");
        System.out.println(" Welcome to Human VS Goblin ");
        System.out.println("############################\n");

        String[][] grid = l.getLand();
        grid[x][y] = "\uD83E\uDD34";
        grid[xg][yg] = "\uD83D\uDC7A";

        new Goblin();
        System.out.println();
        new Human();
        int myhealth = h.getHealth();
        System.out.println();
        l.Printgrid(grid);

        GAME:
        while(true) {
            System.out.println("\n" + "Your next move? n/s/e/w\n");
            String move = "";
            try {
                if (getInput.hasNextInt()) {
                    throw new Exception();
                } else {
                    move = getInput.next();
                    System.out.println();
                }
            } catch (Exception e) {
                System.out.println();
                System.out.println();
                System.out.println("Invalid input!");
                break;
            }
            //String move = getInput.next();


            if (move.equals("n")) {
                if (x <= 1) {
                    x = 1;
                } else  {
                    x -= 2;
                }
            } else if (move.equals("s")) {
                if (x >= 7) {
                    x = 7;
                } else {
                    x += 2;
                }
            } else if (move.equals("e")) {
                if (y >= 9) {
                    y = 9;
                } else {
                    y += 2;
                }
            } else if (move.equals("w")) {
                if (y <= 1) {
                    y = 1;
                } else {
                    y -= 2;
                }
            }

            //Condition where human and goblin collides
            if (x == xg && y == yg) {
                System.out.println("\nA Goblin has appeared!!!\n");
                BATTLE:
                while (true) {
                    System.out.println("------------------------------------------------");

                    int enemyhealth = rand.nextInt(g.getHealth());
                    while (enemyhealth > 0) {
                        System.out.println("\tGoblin's HP: " + enemyhealth);
                        System.out.println("\tYour HP: " + myhealth);
                        System.out.println("\n\tWhat You want to do?");
                        System.out.println("\t1. Attack");
                        System.out.println("\t2. Run");

                        int action;

                        try {
                            System.out.println();
                            System.out.println();
                            action = attack.nextInt();
                        } catch (Exception e) {
                            System.out.println();
                            System.out.println();
                            System.out.println("Invalid input! input should only numbers 1 and 2\n");
                            break GAME;
                        }

                        if(action == 1) {
                            int damageDealt = rand.nextInt(h.getAttackdamge());
                            int damageTaken = rand.nextInt(g.getAttackdamge());

                            enemyhealth  -= damageDealt;
                            myhealth -= damageTaken;

                            System.out.println("\t> You strike the Goblin for " + damageDealt + " damage");
                            System.out.println("\t> You receive " + damageTaken + " in retaliation\n");

                            if (myhealth < 1) {
                                System.out.println("\t> You have taken too much damage! You are too weak to go on!");
                                break;
                            }

                        } else if (action == 2) {
                            System.out.println("\t> You run away from the Goblin\n");
                            xg = xarray[rand.nextInt(xarray.length)];
                            yg = yarray[rand.nextInt(yarray.length)];
                            l.PrintGame(grid, move, x, y);
                            grid[xg][yg] = "\uD83D\uDC7A";
                            l.Printgrid(grid);
                            continue GAME;
                        } else {
                            System.out.println("\t> Invalid command!");
                        }
                    }

                    if (myhealth < 1) {
                        System.out.println("\t> You have died!");
                        break GAME;
                    }

                    System.out.println("------------------------------------------------");
                    System.out.println(" # Goblin was defeated! # ");
                    System.out.println(" # You have " + myhealth + " HP left. #");
                    System.out.println("------------------------------------------------");
                    System.out.println("1. Continue Fighting");
                    System.out.println("2. Exit the game");

                    int action;
                    try {
                        System.out.println();
                        System.out.println();
                        action = attack.nextInt();
                    } catch (Exception e) {
                        System.out.println();
                        System.out.println();
                        System.out.println("Invalid input! input should only numbers 1 and 2\n");
                        break GAME;
                    }

                    //Check to see if the user enters an invalid command
                    while (action != 1 && action != 2) {
                        System.out.println("Invalid command");
                        try {
                            System.out.println();
                            System.out.println();
                            action = attack.nextInt();
                        } catch (Exception e) {
                            System.out.println();
                            System.out.println();
                            System.out.println("Invalid input! input should only numbers 1 and 2\n");
                            break GAME;
                        }
                    }

                    if (action == 1) {
                        System.out.println();
                        System.out.println("You continue to play the game\n");
                        xg = xarray[rand.nextInt(xarray.length)];
                        yg = yarray[rand.nextInt(yarray.length)];
                        l.PrintGame(grid, move, x, y);
                        grid[xg][yg] = "\uD83D\uDC7A";
                        l.Printgrid(grid);
                        continue GAME;
                    } else if (action == 2) {
                        System.out.println("you exit the game!");
                        break GAME;
                    }
                }
            }
            l.PrintGame(grid, move, x, y);
            l.Printgrid(grid);
        }
    }
}
