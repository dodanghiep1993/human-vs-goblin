

public class Goblin {
    private int health;
    private int attackdamge;

    public Goblin (){
        System.out.println(this);
    }
    public Goblin (int health, int attackdamge) {
        this.health = health;
        this.attackdamge = attackdamge;
    }

    public int getHealth() {
        return health;
    }

    public int getAttackdamge() {
        return attackdamge;
    }

    @Override
    public String toString() {
        return "I am Goblin: " + "\uD83D\uDC7A";
    }
}
