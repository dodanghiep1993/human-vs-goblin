

public class Human {
    private int health;
    private int attackdamge;

    public Human (){
        System.out.println(this);
    }
    public Human (int health, int attackdamge) {
        this.health = health;
        this.attackdamge = attackdamge;
    }

    public int getHealth() {
        return health;
    }

    public int getAttackdamge() {
        return attackdamge;
    }

    public void setAttackdamge(int attackdamge) {
        this.attackdamge = attackdamge;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    @Override
    public String toString() {
        return "I am Human: " + "\uD83E\uDD34";
    }
}
